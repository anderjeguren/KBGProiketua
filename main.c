#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "display.h"
#include "io.h"
#include "definitions.h"
#include "matrix.h"

/** GLOBAL VARIABLES **/

GLdouble _window_ratio;                     /*Control of window's proportions */
GLdouble _ortho_x_min,_ortho_x_max;         /*Variables for the control of the orthographic projection*/
GLdouble _ortho_y_min ,_ortho_y_max;        /*Variables for the control of the orthographic projection*/
GLdouble _ortho_z_min,_ortho_z_max;         /*Variables for the control of the orthographic projection*/
GLdouble fovy;
GLdouble aspect;
GLdouble zNear;
GLdouble zFar;

kamera *kameraPerspektiba;
kamera *kameraIbiltaria;
argia *argiaPila;
argia *argiaPila2;
GLdouble ibiltariaAngelua = 0;

object3d * _first_object= 0;                /*List of objects*/
object3d * _selected_object = 0;            /*Object currently selected*/
int egoera=0;
int erreferentzia = 5;
int kameraEgoera = 0;
int argiztapena = 0;
int bonbila = 0;
int fokoa = 0;
GLfloat angelua = 20.0;
int aldaketakNon = KG_ALDAKETAK_OBJEKTUAN;


/** GENERAL INITIALIZATION **/
void initialization (){


    /*Initialization of all the variables with the default values*/
    _ortho_x_min = KG_ORTHO_X_MIN_INIT;
    _ortho_x_max = KG_ORTHO_X_MAX_INIT;
    _ortho_y_min = KG_ORTHO_Y_MIN_INIT;
    _ortho_y_max = KG_ORTHO_Y_MAX_INIT;
    _ortho_z_min = KG_ORTHO_Z_MIN_INIT;
    _ortho_z_max = KG_ORTHO_Z_MAX_INIT;

    _window_ratio = (GLdouble) KG_WINDOW_WIDTH / (GLdouble) KG_WINDOW_HEIGHT;

    /*Definition of the background color*/
    glClearColor(KG_COL_BACK_R, KG_COL_BACK_G, KG_COL_BACK_B, KG_COL_BACK_A);

    /*Definition of the method to draw the objects*/
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    /*The initialization of the perspective camera*/
  	fovy =19;
  	aspect = _window_ratio;
  	zNear = 5;
  	zFar = 100;

    /*The initialization of the fixed camera vectors*/
    GLdouble *eye = (GLdouble*)malloc (sizeof(GLdouble)*4);
        eye[0]=0;   eye[1]=0;   eye[2]=-30; eye[3]=1;
    GLdouble *center = (GLdouble*)malloc (sizeof(GLdouble)*4);
        center[0]=0;    center[1]=0;    center[2]=30; center[3]=1;
    GLdouble *up = (GLdouble*)malloc (sizeof(GLdouble)*4);
        up[0]=0;    up[1]=1;    up[2]=0; up[3]=0;

    /*The initialization of the perspective camera*/
	   kameraPerspektiba = (kamera*)malloc (sizeof(kamera));

    kameraPerspektiba->eye=eye;
	  kameraPerspektiba->center=center;
	  kameraPerspektiba->up=up;

    pila *aldaketaPila;
    aldaketaPila = (pila *) malloc(sizeof (pila));
    aldaketaPila->matrix=kamera_matrix();
    aldaketaPila->next=NULL;
    aldaketaPila->previous=NULL;
    kameraPerspektiba->aldaketa_pila=aldaketaPila;

    /*The initialization of the first player camera*/

    kameraIbiltaria = (kamera*)malloc (sizeof(kamera));

    kameraIbiltaria->eye=eye;
    kameraIbiltaria->center=center;
    kameraIbiltaria->up=up;

    pila *aldaketaPilaIbil;
    aldaketaPilaIbil = (pila *) malloc(sizeof (pila));
    aldaketaPilaIbil->matrix=kamera_matrix();
    aldaketaPilaIbil->next=NULL;
    aldaketaPilaIbil->previous=NULL;
    kameraIbiltaria->aldaketa_pila=aldaketaPilaIbil;

    /*The initialization of the light*/
    GLfloat *kokapena = (GLfloat*)malloc (sizeof(GLfloat)*4);
      kokapena[0]=0.0;   kokapena[1]=10.0;   kokapena[2]=0.0; kokapena[3]=1.0;

    argiaPila = (argia*)malloc (sizeof(argia));
    argiaPila2 = (argia*)malloc (sizeof(argia));

    argiaPila->kokapenaBon=kokapena;
    argiaPila2->kokapenaBon=kokapena;


    pila *aldaketaPilaArg;
    aldaketaPilaArg = (pila *) malloc(sizeof (pila));
    aldaketaPilaArg->matrix=identity_matrix();
    aldaketaPilaArg->next=NULL;
    aldaketaPilaArg->previous=NULL;

    argiaPila->aldaketa_pila=aldaketaPilaArg;
    argiaPila2->aldaketa_pila=aldaketaPilaArg;

}


/** MAIN FUNCTION **/
int main(int argc, char** argv) {

    /*First of all, print the help information*/
    print_help();

    /*Set color to the light*/
    GLfloat horia[4] = {0.0, 1.0, 1.0,  1.0};
    GLfloat grisa[4] = {0.2, 0.2, 0.2,  1.0};
    GLfloat txuria[4] = {1.0, 1.0, 1.0,  1.0};

    /* glut initializations */
    glutInit(&argc, argv);
    glutInitWindowSize(KG_WINDOW_WIDTH, KG_WINDOW_HEIGHT);
    glutInitWindowPosition(KG_WINDOW_X, KG_WINDOW_Y);
    glutCreateWindow(KG_WINDOW_TITLE);
    glutInitDisplayMode(GLUT_RGB|GLUT_SINGLE|GLUT_DEPTH);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);

    /*Argiaren kolorea aplikatu*/
    glLightfv(GL_LIGHT0,GL_AMBIENT,grisa);
    glLightfv(GL_LIGHT0,GL_DIFFUSE,horia);
    glLightfv(GL_LIGHT0,GL_SPECULAR,txuria);


    /* set the callback functions */
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(special_keyboard);

    /* this initialization has to be AFTER the creation of the window */
    initialization();
    /* start the main loop */
    glutMainLoop();
    return 0;
}
