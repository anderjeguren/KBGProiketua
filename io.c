#include "definitions.h"
#include "load_obj.h"
#include "matrix.h"
#include <GL/glut.h>
#include <GL/gl.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "argiak.h"
#include "pila.h"


extern object3d * _first_object;
extern object3d * _selected_object;

extern GLdouble _ortho_x_min,_ortho_x_max;
extern GLdouble _ortho_y_min,_ortho_y_max;
extern GLdouble _ortho_z_min,_ortho_z_max;
extern GLdouble fovy;

extern kamera *kameraPerspektiba;
extern kamera *kameraIbiltaria;
extern GLdouble ibiltariaAngelua;

extern int egoera;
extern int erreferentzia;
extern int kameraEgoera;
extern int argiztapena;
extern GLfloat angelua;
extern int aldaketakNon;
extern int bonbila;
extern int fokoa;

/**
 * @brief This function just prints information about the use
 * of the keys
 */
void print_help(){
    printf("KbG Irakasgaiaren Praktika. Programa honek 3D objektuak \n");
    printf("aldatzen eta bistaratzen ditu.  \n\n");

    printf("Egileak: Josu Arruarte / Ander Juarros \n");
    printf("Data: Azaroak, 2015 \n");
    printf("\n\n");
    printf("FUNTZIO NAGUSIAK \n");
    printf("<?>\t\t Laguntza hau bistaratu \n");
    printf("<ESC>\t\t Programatik irten \n");
    printf("<F>\t\t Objektua bat kargatu\n");
    printf("<I>\t\t Objektuaren informazioa pantailaratu\n");
    printf("<TAB>\t\t Kargaturiko objektuen artean bat hautatu\n");
    printf("<DEL>\t\t Hautatutako objektua ezabatu\n");
    printf("<CTRL + ->\t Bistaratze-eremua handitu\n");
    printf("<CTRL + +>\t Bistaratze-eremua txikitu\n");
    printf("\n\n");
    printf("ALDAKETA FUNTZIOAK \n");
    printf("<M>\t\t Translazioa \n");
    printf("<B>\t\t Biraketa \n");
    printf("<T>\t\t Tamaina \n");
    printf("<CTRL + Z>\t Aldaketa ezabatu\n");
    printf("<G>\t\t Globala \n");
    printf("<L>\t\t Lokala \n");
    printf("<C>\t\t Kamara aldatu \n");
    printf("<K>\t\t Kamaran aldaketak egin \n");
    printf("<O>\t\t Objetuan aldaketak egin \n");
    printf("<CTRL + N>\t Berregin\n");
    printf("GORA\t\t Kamera edo objetua biratu, mugitu, ...\n");
    printf("BEHERA\t\t Kamera edo objetua biratu, mugitu, ...\n");
    printf("EZKER\t\t Kamera edo objetua biratu, mugitu, ...\n");
    printf("ESKUBI\t\t Kamera edo objetua biratu, mugitu, ...\n");
    printf("AVPAG\t\t Kamera edo objetua biratu, mugitu, ...\n");
    printf("REPAG\t\t Kamera edo objetua biratu, mugitu, ...\n");
    printf("\n\n");
    printf("ARGIZTAPEN FUNTZIOAK \n");
    printf("<1,2,3>\t\t Argiztapen mota aldatu(eguzkia, bonbila, fokoa) \n");
    printf("<W>\t\t Argia biratu, mugitu, ...\n");
    printf("<S>\t\t Argia biratu, mugitu, ... \n");
    printf("<A>\t\t Argia biratu, mugitu, ...\n");
    printf("<D>\t\t Argia biratu, mugitu, ...\n");
    printf("<Q>\t\t Fokoaren angelua handitu \n");
    printf("<E>\t\t Fokoaren angelua txikitu \n");
    printf("<F1,F2>\t\t Materialak aldatu \n");
    printf("\n\n");
}

vector3 kalkulatuNormala(point3 *p1, point3 *p2, point3 *p3){
    vector3 v1,v2,v3,v4;
    GLdouble mhelp;

    v1.x=p2->x-p1->x;
    v1.y=p2->y-p1->y;
    v1.z=p2->z-p1->z;

    v2.x=p3->x-p1->x;
    v2.y=p3->y-p1->y;
    v2.z=p3->z-p1->z;

    v3.x=(v1.y*v2.z)-(v1.z*v2.y);
    v3.y=(v1.z*v2.x)-(v1.x*v2.z);
    v3.z=(v1.x*v2.y)-(v1.y*v2.x);

    mhelp = sqrt((v3.x*v3.x)+(v3.y*v3.y)+(v3.z*v3.z));

    v4.x=v3.x/mhelp;
    v4.y=v3.y/mhelp;
    v4.z=v3.z/mhelp;

    return v4;
}

void normalaKalkulatu(object3d * _object){
    int i,j;
    vector3 *normalakH = malloc(sizeof(vector3)*_object->num_faces);
    point3 *aurpegiarenErpinak = malloc(sizeof(point3)*3);
    for(i=0;i<_object->num_faces;i++){
        for(j=0;j<3;j++){
            aurpegiarenErpinak[j]=_object->vertex_table[_object->face_table[i].vertex_table[j]].coord;
        }
        normalakH[i] = kalkulatuNormala(&aurpegiarenErpinak[0],&aurpegiarenErpinak[1],&aurpegiarenErpinak[2]);
    }
    _object->normalak=normalakH;
 }

/**
 * @brief Callback function to control the basic keys
 * @param key Key that has been pressed
 * @param x X coordinate of the mouse pointer when the key was pressed
 * @param y Y coordinate of the mouse pointer when the key was pressed
 */
void keyboard(unsigned char key, int x, int y){

    double pi = PI;
    char* fname = malloc(sizeof (char)*128); /* Note that scanf adds a null character at the end of the vector*/
    int read = 0;
    object3d *auxiliar_object = 0;
    GLdouble wd,he,midx,midy;

    switch (key) {
    case 'f':
    case 'F':
        /*Ask for file*/
        printf("%s", KG_MSSG_SELECT_FILE);
        scanf("%s", fname);
        auxiliar_object = (object3d *) malloc(sizeof (object3d));
        read = read_wavefront(fname, auxiliar_object);
        switch (read) {
        /*Errors in the reading*/
        case 1:
            printf("%s: %s\n", fname, KG_MSSG_FILENOTFOUND);
            break;
        case 2:
            printf("%s: %s\n", fname, KG_MSSG_INVALIDFILE);
            break;
        case 3:
            printf("%s: %s\n", fname, KG_MSSG_EMPTYFILE);
            break;
        /*Read OK*/
        case 0:
            /*Insert the new object in the list*/
            auxiliar_object->next = _first_object;
            _first_object = auxiliar_object;
            _selected_object = _first_object;
            printf("%s\n",KG_MSSG_FILEREAD);
            break;
        }
        break;

    case 'i':
    case 'I':
    	/*If there is not an object in the list. */
    	if (_selected_object == NULL)
	{
		printf("Ez da irudirik kargartu.\n");
		break;
	}
	else
	{
		char inprimatzeko[1024];
		sprintf(inprimatzeko,"Aurpegi kopurua: %d | Erpin kopurua: %d",_selected_object->num_faces,_selected_object->num_vertices);

		printf("%s\n",inprimatzeko);
	}

    	break;

    case 9: /* <TAB> */
	/*If there is not an object in the list. */
	if (_selected_object == NULL)
	{
		printf("Ez da irudirik kargartu. \n");
		break;
	}
	else{

      		 _selected_object = _selected_object->next;
       		 /*The selection is circular, thus if we move out of the list we go back to the first element*/
        	 if (_selected_object == 0) _selected_object = _first_object;
       		 break;
	}
    case 127: /* <SUPR> */
        /*Erasing an object depends on whether it is the first one or not*/
	if (_selected_object == NULL)
	{

		printf("Ez da irudirik kargartu.\n");
		break;
	}
        else if (_selected_object == _first_object)
 	{
            /*To remove the first object we just set the first as the current's next*/
            _first_object = _first_object->next;
            /*Once updated the pointer to the first object it is save to free the memory*/
            free(_selected_object);
            /*Finally, set the selected to the new first one*/
            _selected_object = _first_object;
        } else {
            /*In this case we need to get the previous element to the one we want to erase*/
            auxiliar_object = _first_object;
            while (auxiliar_object->next != _selected_object)
                auxiliar_object = auxiliar_object->next;
            /*Now we bypass the element to erase*/
            auxiliar_object->next = _selected_object->next;
            /*free the memory*/
            free(_selected_object);
            /*and update the selection*/
            _selected_object = auxiliar_object;
        }
        printf("Irudia ezabatu da.\n");
        break;

    case '-':
        if (glutGetModifiers() == GLUT_ACTIVE_CTRL){
            /*Increase the projection plane; compute the new dimensions*/
            wd=(_ortho_x_max-_ortho_x_min)/KG_STEP_ZOOM;
            he=(_ortho_y_max-_ortho_y_min)/KG_STEP_ZOOM;
            /*In order to avoid moving the center of the plane, we get its coordinates*/
            midx = (_ortho_x_max+_ortho_x_min)/2;
            midy = (_ortho_y_max+_ortho_y_min)/2;
            /*The the new limits are set, keeping the center of the plane*/
            _ortho_x_max = midx + wd/2;
            _ortho_x_min = midx - wd/2;
            _ortho_y_max = midy + he/2;
            _ortho_y_min = midy - he/2;
        }
	 if (glutGetModifiers() == GLUT_ACTIVE_CTRL) fovy=fovy+3;
        break;

    case '+':
        if (glutGetModifiers() == GLUT_ACTIVE_CTRL){
            /*Increase the projection plane; compute the new dimensions*/
            wd=(_ortho_x_max-_ortho_x_min)*KG_STEP_ZOOM;
            he=(_ortho_y_max-_ortho_y_min)*KG_STEP_ZOOM;
            /*In order to avoid moving the center of the plane, we get its coordinates*/
            midx = (_ortho_x_max+_ortho_x_min)/2;
            midy = (_ortho_y_max+_ortho_y_min)/2;
            /*The the new limits are set, keeping the center of the plane*/
            _ortho_x_max = midx + wd/2;
            _ortho_x_min = midx - wd/2;
            _ortho_y_max = midy + he/2;
            _ortho_y_min = midy - he/2;
        }

 	if (glutGetModifiers() == GLUT_ACTIVE_CTRL) fovy=fovy-3;

        break;

        case 26 : /* <Ctrl Z */
    /*Go back in the pile to the last change*/
        if(aldaketakNon==KG_ALDAKETAK_KAMERAN){
            if(kameraEgoera==KG_PERSPEKTIBA){
                if(kameraPerspektiba->aldaketa_pila->previous == NULL){
                    printf("Ez da aldaketarik egin. \n");
                }else{
                    pila *pilaBerria;
                    pilaBerria = kameraPerspektiba->aldaketa_pila;
                    kameraPerspektiba->aldaketa_pila = kameraPerspektiba->aldaketa_pila->previous;
                    kameraPerspektiba->aldaketa_pila->next = pilaBerria;
                    eguneratu_kameraPerspektiba();
                }
            }
        }else{
            if (_selected_object == NULL)
            {
                printf("Ez da irudirik kargartu. \n");
                break;
            }
            else if(_selected_object->aldaketa_pila->previous == NULL){
                printf("Ez da aldaketarik egin. \n");
            }else{
                pila *pilaBerria;
                pilaBerria = _selected_object->aldaketa_pila;
                _selected_object->aldaketa_pila = _selected_object->aldaketa_pila->previous;
                _selected_object->aldaketa_pila->next = pilaBerria;
            }
        }
        break;

    case 14 : /* <Ctrl N */
   	/*Go to the next change*/
       if(aldaketakNon==KG_ALDAKETAK_KAMERAN){
            if(kameraEgoera==KG_PERSPEKTIBA){
                if(kameraPerspektiba->aldaketa_pila->next == NULL){
                    printf("Ez da aldaketarik egin. \n");
                }else{
                    pila *pilaBerria;
                    pilaBerria = kameraPerspektiba->aldaketa_pila;
                    kameraPerspektiba->aldaketa_pila = kameraPerspektiba->aldaketa_pila->next;
                    kameraPerspektiba->aldaketa_pila->previous = pilaBerria;
                    eguneratu_kameraPerspektiba();
                }
            }
        }else{
            if (_selected_object == NULL)
            {
                printf("Ez da irudirik kargartu. \n");
                break;
            }
            else if(_selected_object->aldaketa_pila->next == NULL){
                printf("Ez da aldaketarik egin. \n");
            }else{
                pila *pilaBerria;
                pilaBerria = _selected_object->aldaketa_pila;
                _selected_object->aldaketa_pila = _selected_object->aldaketa_pila->next;
                _selected_object->aldaketa_pila->previous = pilaBerria;
            }
        }
        break;

    case 'k':
    case 'K':
    /*Kamaran aldaketak eragin*/
        aldaketakNon=KG_ALDAKETAK_KAMERAN;
        printf("Aldaketak kameran egingo dira \n");
        break;

    case 'o':
    case 'O':
    /*Objetuan aldaketak eragin*/
        aldaketakNon=KG_ALDAKETAK_OBJEKTUAN;
        printf("Aldaketak objektuan egingo dira \n");
        break;

    case '?':
        print_help();
        break;

    case 'm':
    case 'M':
    	/* Aldaketa egoera Translazioa */
    	egoera=KG_TRANSLAZIO_EG;
    	break;
    case 'b':
    case 'B':
    	/* Aldaketa egoera Biraketa */
	   egoera=KG_BIRAKETA_EG;
    	break;

    case 't':
    case 'T':
    	/* Aldaketa egoera Tamaina */
    	egoera=KG_TAMAINA_EG;
    	break;

    case 'l':
    case 'L':
    	/* Aldaketa erreferentzia Lokala */
    	erreferentzia=KG_LOCAL;
    	break;

    case 'g':
    case 'G':
    	/* Aldaketa erreferentzia Globala */
    	erreferentzia=KG_GLOBAL;
    	break;

    case 'c':
    case 'C':
    	/* kameraEgoera aldatu  */
    	if(kameraEgoera==KG_ORTOGRAFIKOA){
            kameraEgoera=KG_PERSPEKTIBA;
            printf("Kamera Perspektiba\n");
        }
    	else if(kameraEgoera==KG_PERSPEKTIBA){
            kameraEgoera=KG_BIDEOJOKOA;
            printf("Kamera Ibiltaria\n");
        }
    	else if(kameraEgoera==KG_BIDEOJOKOA){
            kameraEgoera=KG_ORTOGRAFIKOA;
            printf("Kamera Ortogonala\n");
        }
    	break;
    case 'w':
    case 'W':
    /*Argia biratu*/
    if(fokoa==1)
      mugituFokua(translation_matrix(0,1,0),angelua);
    if(bonbila==1)
      biratuBonbila(rotation_matrixX(-pi/10));
      break;

    case 's':
    case 'S':
    /*Argia biratu*/
    if(fokoa==1)
      mugituFokua(translation_matrix(0,-1,0),angelua);
    if(bonbila==1)
      biratuBonbila(rotation_matrixX(pi/10));
      break;

    case 'a':
    case 'A':
    /*Argia biratu*/
    if(fokoa==1)
      mugituFokua(translation_matrix(-1,0,0),angelua);
    if(bonbila==1)
      biratuBonbila(rotation_matrixZ(pi/10));
      break;

    case 'd':
    case 'D':
    /*Argia biratu*/
    if(fokoa==1)
      mugituFokua(translation_matrix(1,0,0),angelua);
    if(bonbila==1)
      biratuBonbila(rotation_matrixZ(-pi/10));
      break;

    case 'e':
    case 'E':
    /*Fokoaren angelua murriztu*/
    if(fokoa==1){
      angelua=angelua-1;
      fokoaPiztu(angelua);
    }
      break;

    case 'q':
    case 'Q':
    /*Fokoaren angelua handitu*/
    if(fokoa==1){
      angelua=angelua+1;
      fokoaPiztu(angelua);
    }
      break;

    case 13: /* <ENTER> */
        /* Argiak piztu/itzali  */
        if(argiztapena==0){
          argiztapena=1;
          glEnable(GL_LIGHTING);
          glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
          eguzkiaPiztu();
          break;
        }
        else if(argiztapena==1){
          argiztapena=0;
          glDisable(GL_LIGHTING);
          glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
          break;
        }

    case 49: /* <Nº1> */
        /* Argiztapena aldatu  */
          eguzkiaPiztu();
          fokoa=0;
          bonbila=0;
          break;

    case 50: /* <Nº2> */
        /* Argiztapena aldatu  */
         bonbilaPiztu();
         fokoa=0;
         bonbila=1;
        break;
    case 51: /* <Nº3> */
        /* Argiztapena aldatu  */
         fokoaPiztu(angelua);
         fokoa=1;
         bonbila=0;
        break;
    case 27: /* <ESC> */
        exit(0);
        break;

    default:
        /*In the default case we just print the code of the key. This is usefull to define new cases*/
        printf("%d %c\n", key, key);
    }
    /*In case we have do any modification affecting the displaying of the object, we redraw them*/
    glutPostRedisplay();
}
/**
 * @brief Callback function to control the special keysb
 * @param key Key that has been pressed
 * @param x X coordinate of the mouse pointer when the key was pressed
 * @param y Y coordinate of the mouse pointer when the key was pressed
 */

void special_keyboard(int key, int x, int y) {

    double pi = PI;
    switch(key){
    case GLUT_KEY_LEFT: /* Arrow key left */
        if(aldaketakNon==KG_ALDAKETAK_KAMERAN){
            if(kameraEgoera==KG_BIDEOJOKOA){
                if(kameraIbiltaria->aldaketa_pila->previous!=NULL)
                kameraIbiltaria->aldaketa_pila = kameraIbiltaria->aldaketa_pila->previous;
                kameraIbiltaria->aldaketa_pila = eguneratu_objetua(rotation_matrixY(-pi/40),kameraIbiltaria->aldaketa_pila);
                kameraIbiltaria->aldaketa_pila = eguneratu_objetua(rotation_matrixX(ibiltariaAngelua),kameraIbiltaria->aldaketa_pila);
                eguneratu_kameraIbiltaria();
            }
            else if(kameraEgoera==KG_PERSPEKTIBA){
                switch (egoera){
                    case KG_TRANSLAZIO_EG: /* Translazioa */
                        if(erreferentzia==KG_LOCAL) kameraPerspektiba->aldaketa_pila = eguneratu_objetua(translation_matrix(-1,0,0),kameraPerspektiba->aldaketa_pila);
                        else if(erreferentzia==KG_GLOBAL) kameraPerspektiba->aldaketa_pila = eguneratu_mundua(translation_matrix(-1,0,0),kameraPerspektiba->aldaketa_pila);
                        eguneratu_kameraPerspektiba();

                        break;

                     case KG_BIRAKETA_EG: /* Biraketa */
                        if(erreferentzia==KG_LOCAL) kameraPerspektiba->aldaketa_pila = eguneratu_objetua(rotation_matrixY(-pi/10),kameraPerspektiba->aldaketa_pila);
                        else if(erreferentzia==KG_GLOBAL) kameraPerspektiba->aldaketa_pila = eguneratu_mundua(rotation_matrixY(-pi/10),kameraPerspektiba->aldaketa_pila);
                        eguneratu_kameraPerspektiba();

                        break;
                }
            }
        }
        else{
            if (_selected_object == NULL)
            {
                printf("Ez da irudirik kargartu. \n");
                break;
            }
            switch (egoera){
                case KG_TRANSLAZIO_EG: /* Translazioa */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(translation_matrix(-1,0,0),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(translation_matrix(-1,0,0),_selected_object->aldaketa_pila);
                    break;

                 case KG_BIRAKETA_EG: /* Biraketa */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(rotation_matrixY(-pi/10),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(rotation_matrixY(-pi/10),_selected_object->aldaketa_pila);
                    break;
                 case KG_TAMAINA_EG: /* Tamaina */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(tamaina_matrix(1.2,1,1),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(tamaina_matrix(1.2,1,1),_selected_object->aldaketa_pila);
                    break;
            }
        }
        break;

    case GLUT_KEY_RIGHT: /* Arrow key right */
        if(aldaketakNon==KG_ALDAKETAK_KAMERAN){
            if(kameraEgoera==KG_BIDEOJOKOA){
                if(kameraIbiltaria->aldaketa_pila->previous!=NULL)
                kameraIbiltaria->aldaketa_pila = kameraIbiltaria->aldaketa_pila->previous;
                kameraIbiltaria->aldaketa_pila = eguneratu_objetua(rotation_matrixY(pi/40),kameraIbiltaria->aldaketa_pila);
                kameraIbiltaria->aldaketa_pila = eguneratu_objetua(rotation_matrixX(ibiltariaAngelua),kameraIbiltaria->aldaketa_pila);
                eguneratu_kameraIbiltaria();
            }
            else if(kameraEgoera==KG_PERSPEKTIBA){
                switch (egoera){
                    case KG_TRANSLAZIO_EG: /* Translazioa */
                        if(erreferentzia==KG_LOCAL) kameraPerspektiba->aldaketa_pila = eguneratu_objetua(translation_matrix(1,0,0),kameraPerspektiba->aldaketa_pila);
                        else if(erreferentzia==KG_GLOBAL) kameraPerspektiba->aldaketa_pila = eguneratu_mundua(translation_matrix(1,0,0),kameraPerspektiba->aldaketa_pila);
                        eguneratu_kameraPerspektiba();

                        break;

                     case KG_BIRAKETA_EG: /* Biraketa */
                        if(erreferentzia==KG_LOCAL) kameraPerspektiba->aldaketa_pila = eguneratu_objetua(rotation_matrixY(pi/10),kameraPerspektiba->aldaketa_pila);
                        else if(erreferentzia==KG_GLOBAL) kameraPerspektiba->aldaketa_pila = eguneratu_mundua(rotation_matrixY(pi/10),kameraPerspektiba->aldaketa_pila);
                        eguneratu_kameraPerspektiba();

                        break;
                }
            }
        }
        else{
            if (_selected_object == NULL)
            {
                printf("Ez da irudirik kargartu. \n");
                break;
            }
            switch (egoera){
                case KG_TRANSLAZIO_EG: /* Translazioa */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(translation_matrix(1,0,0),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(translation_matrix(1,0,0),_selected_object->aldaketa_pila);
                    break;

                case KG_BIRAKETA_EG: /* Biraketa */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(rotation_matrixY(pi/10),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(rotation_matrixY(pi/10),_selected_object->aldaketa_pila);
                    break;
               case KG_TAMAINA_EG: /* Tamaina */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(tamaina_matrix(0.8,1,1),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(tamaina_matrix(0.8,1,1),_selected_object->aldaketa_pila);
                    break;
            }
        }
        break;

    case GLUT_KEY_UP: /* Arrow key up */
        if(aldaketakNon==KG_ALDAKETAK_KAMERAN){
            if(kameraEgoera==KG_BIDEOJOKOA){
                        if(kameraIbiltaria->aldaketa_pila->previous!=NULL)
                        kameraIbiltaria->aldaketa_pila = kameraIbiltaria->aldaketa_pila->previous;
                        kameraIbiltaria->aldaketa_pila = eguneratu_objetua(translation_matrix(0,0,1),kameraIbiltaria->aldaketa_pila);
                        kameraIbiltaria->aldaketa_pila = eguneratu_objetua(rotation_matrixX(ibiltariaAngelua),kameraIbiltaria->aldaketa_pila);
                        eguneratu_kameraIbiltaria();
            }
            else if(kameraEgoera==KG_PERSPEKTIBA){
                switch (egoera){
                    case KG_TRANSLAZIO_EG: /* Translazioa */
                        if(erreferentzia==KG_LOCAL) kameraPerspektiba->aldaketa_pila = eguneratu_objetua(translation_matrix(0,1,0),kameraPerspektiba->aldaketa_pila);
                        else if(erreferentzia==KG_GLOBAL) kameraPerspektiba->aldaketa_pila = eguneratu_mundua(translation_matrix(0,1,0),kameraPerspektiba->aldaketa_pila);
                        eguneratu_kameraPerspektiba();

                        break;

                     case KG_BIRAKETA_EG: /* Biraketa */
                        if(erreferentzia==KG_LOCAL) kameraPerspektiba->aldaketa_pila = eguneratu_objetua(rotation_matrixX(-pi/10),kameraPerspektiba->aldaketa_pila);
                        else if(erreferentzia==KG_GLOBAL) kameraPerspektiba->aldaketa_pila = eguneratu_mundua(rotation_matrixX(-pi/10),kameraPerspektiba->aldaketa_pila);
                        eguneratu_kameraPerspektiba();

                        break;
                }
            }
        }
        else{
            if (_selected_object == NULL)
            {
                printf("Ez da irudirik kargartu. \n");
                break;
            }
            switch (egoera){
                case KG_TRANSLAZIO_EG: /* Translazioa */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(translation_matrix(0,1,0),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(translation_matrix(0,1,0),_selected_object->aldaketa_pila);
                    break;

                 case KG_BIRAKETA_EG: /* Biraketa */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(rotation_matrixX(-pi/10),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(rotation_matrixX(-pi/10),_selected_object->aldaketa_pila);
                    break;
                 case KG_TAMAINA_EG: /* Tamaina */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(tamaina_matrix(1,1.2,1),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(tamaina_matrix(1,1.2,1),_selected_object->aldaketa_pila);
                    break;
            }
        }
        break;

    case GLUT_KEY_DOWN: /* Arrow key down */
        if(aldaketakNon==KG_ALDAKETAK_KAMERAN){
            if(kameraEgoera==KG_BIDEOJOKOA){
                if(kameraIbiltaria->aldaketa_pila->previous!=NULL)
                kameraIbiltaria->aldaketa_pila = kameraIbiltaria->aldaketa_pila->previous;
                kameraIbiltaria->aldaketa_pila = eguneratu_objetua(translation_matrix(0,0,-1),kameraIbiltaria->aldaketa_pila);
                kameraIbiltaria->aldaketa_pila = eguneratu_objetua(rotation_matrixX(ibiltariaAngelua),kameraIbiltaria->aldaketa_pila);
                eguneratu_kameraIbiltaria();
            }
            else if(kameraEgoera==KG_PERSPEKTIBA){
                switch (egoera){
                    case KG_TRANSLAZIO_EG: /* Translazioa */
                        if(erreferentzia==KG_LOCAL) kameraPerspektiba->aldaketa_pila = eguneratu_objetua(translation_matrix(0,-1,0),kameraPerspektiba->aldaketa_pila);
                        else if(erreferentzia==KG_GLOBAL) kameraPerspektiba->aldaketa_pila = eguneratu_mundua(translation_matrix(0,-1,0),kameraPerspektiba->aldaketa_pila);
                        eguneratu_kameraPerspektiba();

                        break;

                     case KG_BIRAKETA_EG: /* Biraketa */
                        if(erreferentzia==KG_LOCAL) kameraPerspektiba->aldaketa_pila = eguneratu_objetua(rotation_matrixX(pi/10),kameraPerspektiba->aldaketa_pila);
                        else if(erreferentzia==KG_GLOBAL) kameraPerspektiba->aldaketa_pila = eguneratu_mundua(rotation_matrixX(pi/10),kameraPerspektiba->aldaketa_pila);
                        eguneratu_kameraPerspektiba();

                        break;
                }
            }
        }
        else{
            if (_selected_object == NULL)
            {
                printf("Ez da irudirik kargartu. \n");
                break;
            }
            switch (egoera){
                case KG_TRANSLAZIO_EG: /* Translazioa */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(translation_matrix(0,-1,0),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(translation_matrix(0,1,0),_selected_object->aldaketa_pila);
                    break;
                 case KG_BIRAKETA_EG: /* Biraketa */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(rotation_matrixX(pi/10),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(rotation_matrixX(pi/10),_selected_object->aldaketa_pila);
                    break;
                 case KG_TAMAINA_EG: /* Tamaina */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(tamaina_matrix(1,0.8,1),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(tamaina_matrix(1,0.8,1),_selected_object->aldaketa_pila);
                    break;
            }
        }
        break;

    case GLUT_KEY_PAGE_UP:
        if(aldaketakNon==KG_ALDAKETAK_KAMERAN){
            if(kameraEgoera==KG_BIDEOJOKOA){
                ibiltariaAngelua += -pi/40;
                if(ibiltariaAngelua < 10*(-pi/40)) ibiltariaAngelua -= -pi/40;
                if(kameraIbiltaria->aldaketa_pila->previous!=NULL)
                kameraIbiltaria->aldaketa_pila = kameraIbiltaria->aldaketa_pila->previous;
                kameraIbiltaria->aldaketa_pila = eguneratu_objetua(rotation_matrixX(ibiltariaAngelua),kameraIbiltaria->aldaketa_pila);
                eguneratu_kameraIbiltaria();
            }
            else if(kameraEgoera==KG_PERSPEKTIBA){
                switch (egoera){
                    case KG_TRANSLAZIO_EG: /* Translazioa */
                        if(erreferentzia==KG_LOCAL) kameraPerspektiba->aldaketa_pila = eguneratu_objetua(translation_matrix(0,0,1),kameraPerspektiba->aldaketa_pila);
                        else if(erreferentzia==KG_GLOBAL) kameraPerspektiba->aldaketa_pila = eguneratu_mundua(translation_matrix(0,0,1),kameraPerspektiba->aldaketa_pila);
                        eguneratu_kameraPerspektiba();

                        break;

                     case KG_BIRAKETA_EG: /* Biraketa */
                        if(erreferentzia==KG_LOCAL) kameraPerspektiba->aldaketa_pila = eguneratu_objetua(rotation_matrixX(-pi/10),kameraPerspektiba->aldaketa_pila);
                        else if(erreferentzia==KG_GLOBAL) kameraPerspektiba->aldaketa_pila = eguneratu_mundua(rotation_matrixX(-pi/10),kameraPerspektiba->aldaketa_pila);
                        eguneratu_kameraPerspektiba();

                        break;
                }
            }
        }
        else{
            if (_selected_object == NULL)
            {
                printf("Ez da irudirik kargartu. \n");
                break;
            }
            switch (egoera){
                case KG_TRANSLAZIO_EG: /* Translazioa */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(translation_matrix(0,0,1),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(translation_matrix(0,0,1),_selected_object->aldaketa_pila);
                    break;

                 case KG_BIRAKETA_EG: /* Biraketa */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(rotation_matrixZ(-pi/10),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(rotation_matrixZ(-pi/10),_selected_object->aldaketa_pila);
                    break;
                 case KG_TAMAINA_EG: /* Tamaina */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(tamaina_matrix(1,1,1.2),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(tamaina_matrix(1,1,1.2),_selected_object->aldaketa_pila);
                    break;
            }
        }
        break;

    case GLUT_KEY_PAGE_DOWN:
        if(aldaketakNon==KG_ALDAKETAK_KAMERAN){
            if(kameraEgoera==KG_BIDEOJOKOA){
                ibiltariaAngelua -= -pi/40;
                if(ibiltariaAngelua > 10*(pi/40)) ibiltariaAngelua += -pi/40;
                if(kameraIbiltaria->aldaketa_pila->previous!=NULL)
                kameraIbiltaria->aldaketa_pila = kameraIbiltaria->aldaketa_pila->previous;
                kameraIbiltaria->aldaketa_pila = eguneratu_objetua(rotation_matrixX(ibiltariaAngelua),kameraIbiltaria->aldaketa_pila);
                eguneratu_kameraIbiltaria();
            }
            else if(kameraEgoera==KG_PERSPEKTIBA){
                switch (egoera){
                    case KG_TRANSLAZIO_EG: /* Translazioa */
                        if(erreferentzia==KG_LOCAL) kameraPerspektiba->aldaketa_pila = eguneratu_objetua(translation_matrix(0,0,-1),kameraPerspektiba->aldaketa_pila);
                        else if(erreferentzia==KG_GLOBAL) kameraPerspektiba->aldaketa_pila = eguneratu_mundua(translation_matrix(0,0,-1),kameraPerspektiba->aldaketa_pila);
                        eguneratu_kameraPerspektiba();

                        break;

                     case KG_BIRAKETA_EG: /* Biraketa */
                        if(erreferentzia==KG_LOCAL) kameraPerspektiba->aldaketa_pila = eguneratu_objetua(rotation_matrixZ(pi/10),kameraPerspektiba->aldaketa_pila);
                        else if(erreferentzia==KG_GLOBAL) kameraPerspektiba->aldaketa_pila = eguneratu_mundua(rotation_matrixZ(pi/10),kameraPerspektiba->aldaketa_pila);
                        eguneratu_kameraPerspektiba();

                        break;
                }
            }
        }
        else{
            if (_selected_object == NULL)
            {
                printf("Ez da irudirik kargartu. \n");
                break;
            }
            switch (egoera){
                case KG_TRANSLAZIO_EG: /* Translazioa */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(translation_matrix(0,0,-1),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(translation_matrix(0,0,-1),_selected_object->aldaketa_pila);
                    break;

                 case KG_BIRAKETA_EG: /* Biraketa */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(rotation_matrixZ(pi/10),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(rotation_matrixZ(pi/10),_selected_object->aldaketa_pila);
                    break;
                 case KG_TAMAINA_EG: /* Tamaina */
                    if(erreferentzia==KG_LOCAL) _selected_object->aldaketa_pila = eguneratu_objetua(tamaina_matrix(1,1,0.8),_selected_object->aldaketa_pila);
                    else if(erreferentzia==KG_GLOBAL) _selected_object->aldaketa_pila = eguneratu_mundua(tamaina_matrix(1,1,0.8),_selected_object->aldaketa_pila);
                    break;
            }
        }
        break;
      case GLUT_KEY_F1:
        /* F1 sakatzean */
        printf("Kobre materiala. \n");
        kobreMateriala();
        break;
      case GLUT_KEY_F2:
       /* F2 sakatzean */
       printf("Ruby materiala. \n");
        jadeMateriala();
        break;
    }
     glutPostRedisplay();
}
