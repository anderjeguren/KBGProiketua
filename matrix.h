#ifndef MATRIX_H
#define	MATRIX_H

#include "definitions.h"

GLdouble* translation_matrix(int x,int y,int z);

GLdouble* identity_matrix();
GLdouble* kamera_matrix();

GLdouble* rotation_matrixX(double x);
GLdouble* rotation_matrixY(double y);
GLdouble* rotation_matrixZ(double z);

GLdouble* tamaina_matrix(double x,double y,double z);

void printMatrix(GLdouble* matrizea);

#endif //MATRIX_H
