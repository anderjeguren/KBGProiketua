#include "definitions.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "pila.h"
#include "matrix.h"

extern argia *argiaPila;
extern argia *argiaPila2;

/* Eguzkia aktibatzeko funtzioa(beti posizio eta norabide berdinarekin)*/
void eguzkiaPiztu(){
  GLfloat  norabidea  [4] = {1.0, 0.0, 0.0, 0.0};
  glLightfv(GL_LIGHT0,GL_POSITION,norabidea);
  glLightf(GL_LIGHT0,GL_SPOT_CUTOFF,180.0);
}

/* Bonbila aktibatzeko funtzioa*/
void bonbilaPiztu(int posizioa){
  glLightfv(GL_LIGHT0,GL_POSITION,argiaPila->kokapenaBon);
  glLightf(GL_LIGHT0,GL_SPOT_CUTOFF,180.0);
  }

/* Bonbilaren posizioa aldatzeko funtzioa(biratu)*/
void biratuBonbila(GLdouble *biraketa){
 argiaPila->aldaketa_pila=eguneratu_objetua(biraketa,argiaPila->aldaketa_pila);
 GLfloat  kokapenaLag  [4] = {0.0, 10.0, 0.0, 1.0};
 argiaPila->kokapenaBon=biderkatuArgia(kokapenaLag,argiaPila->aldaketa_pila);

 glLightfv(GL_LIGHT0,GL_POSITION,argiaPila->kokapenaBon);
 glLightf(GL_LIGHT0,GL_SPOT_CUTOFF,180.0);
}

/* Fokoa aktibatzeko funtzioa(honen angelua aldatu zehaztu daiteke)*/
void fokoaPiztu(GLfloat angelua){
  GLfloat  kokapena  [4] = {0.0, 10.0, 0.0, 1.0};
  GLfloat  norabidea  [4] = {0.0,  -1.0, 0.0};
  glLightfv(GL_LIGHT0,GL_POSITION,argiaPila2->kokapenaBon);
  glLightfv(GL_LIGHT0,GL_SPOT_DIRECTION,norabidea);
  glLightf(GL_LIGHT0,GL_SPOT_CUTOFF,angelua);
  glLightf(GL_LIGHT0,GL_SPOT_EXPONENT,1.0);
}

/* Bonbilaren posizioa aldatzeko funtzioa(biratu)*/
void mugituFokua(GLdouble *aldaketa,GLfloat angelua){
 argiaPila2->aldaketa_pila=eguneratu_objetua(aldaketa,argiaPila2->aldaketa_pila);
 GLfloat  kokapenaLag  [4] = {0.0, 10.0, 0.0, 1.0};
 argiaPila2->kokapenaBon=biderkatuArgia(kokapenaLag,argiaPila2->aldaketa_pila);

 GLfloat  norabidea  [4] = {0.0,  -1.0, 0.0};
 glLightfv(GL_LIGHT0,GL_POSITION,argiaPila2->kokapenaBon);
 glLightfv(GL_LIGHT0,GL_SPOT_DIRECTION,norabidea);
 glLightf(GL_LIGHT0,GL_SPOT_CUTOFF,angelua);
 glLightf(GL_LIGHT0,GL_SPOT_EXPONENT,1.0);
}

void kobreMateriala(){
  glDisable(GL_COLOR_MATERIAL);
  GLfloat  ambient [4] = {0.19125 ,  0.0735 ,  0.0225 ,  1.0};
  GLfloat  diffuse [4] = {0.7038 ,  0.27048 ,  0.0828 ,  1.0};
  GLfloat  specular [4] = {0.256777 ,  0.137622 ,  0.0806014 ,  1.0};
  glMaterialfv(GL_FRONT,GL_AMBIENT,ambient);
  glMaterialfv(GL_FRONT,GL_DIFFUSE,diffuse);
  glMaterialfv(GL_FRONT,GL_SPECULAR,specular);
  glMaterialf(GL_FRONT,GL_SHININESS,0.1);

}

void jadeMateriala(){
  glDisable(GL_COLOR_MATERIAL);
  GLfloat  ambient [4] = {0.1745 ,  0.01175 ,  0.01175 ,  1.0};
  GLfloat  diffuse [4] = {0.61424 ,  0.04136 ,  0.04136 ,  1.0};
  GLfloat  specular [4] = {0.727811 ,  0.626959 ,  0.626959 ,  1.0};
  glMaterialfv(GL_FRONT,GL_AMBIENT,ambient);
  glMaterialfv(GL_FRONT,GL_DIFFUSE,diffuse);
  glMaterialfv(GL_FRONT,GL_SPECULAR,specular);
  glMaterialf(GL_FRONT,GL_SHININESS,0.6);

}
