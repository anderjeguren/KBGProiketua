#ifndef IO_H
#define IO_H
#define MAX_BUF 1024

#include "definitions.h"

void keyboard(unsigned char key, int x, int y);
void special_keyboard(int key, int x, int y);
void print_help();
void normalaKalkulatu(object3d * _selected_object);


#endif // IO_H
