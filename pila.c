#include "definitions.h"
#include "load_obj.h"
#include "matrix.h"
#include <GL/glut.h>
#include <GL/gl.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "argiak.h"

extern kamera *kameraPerspektiba;
extern kamera *kameraIbiltaria;

/* Objetuko pila eguneratu(lokala)*/
pila* eguneratu_objetua(GLdouble *matrix,pila *pilaZaharra){
    GLdouble *lag =pilaZaharra->matrix;
    GLdouble batura=0;
    GLdouble *newMatrix = (GLdouble*)malloc (sizeof(GLdouble)*16);
    int i,j,z;
    /*Super Algoritmoa*/
    for(i=0;i<4;i++){
        for(j=0;j<4;j++){
            for(z=0;z<4;z++){
                batura=batura + lag[i+4*z]*matrix[j*4+z];
            }
            newMatrix[i+4*j]=batura;
            batura=0;
        }
    }
    pila *pilaBerria;
    pilaBerria = (pila *) malloc(sizeof (pila));
    pilaBerria->matrix = newMatrix;
    pilaBerria->previous = pilaZaharra;
    return pilaBerria;
}

/* Objetuko pila eguneratu(mundua)*/
pila* eguneratu_mundua(GLdouble *matrix,pila *pilaZaharra){
  GLdouble *lag =pilaZaharra->matrix;
    GLdouble batura=0;
  GLdouble *newMatrix = (GLdouble*)malloc (sizeof(GLdouble)*16);
    int i,j,z;
    /*Super Algoritmoa*/
    for(i=0;i<4;i++){
        for(j=0;j<4;j++){
            for(z=0;z<4;z++){
                batura=batura + matrix[i+4*z]*lag[j*4+z];
            }
            newMatrix[i+4*j]=batura;
            batura=0;
        }
    }
    pila *pilaBerria;
    pilaBerria = (pila *) malloc(sizeof (pila));
    pilaBerria->matrix = newMatrix;
    pilaBerria->previous = pilaZaharra;
    return pilaBerria;
}

/* Bektorea eta matrizea biderkatu*/
GLdouble* biderkatuBektorea(GLdouble *bektorea,pila *pila){
    GLdouble *lag =pila->matrix;
    GLdouble batura=0;
    GLdouble *bektoreBerria = (GLdouble*)malloc (sizeof(GLdouble)*4);
    int i,j;
    for(i=0;i<4;i++){
        for(j=0;j<4;j++){
            batura=batura + lag[i+j*4]*bektorea[j];
        }
        bektoreBerria[i]=batura;
        batura=0;
    }
    return bektoreBerria;
}

/* Bektorea eta matrizea biderkatu(float)*/
GLfloat* biderkatuArgia(GLfloat *bektorea,pila *pila){
    GLdouble *lag =pila->matrix;
    GLfloat batura=0;
    GLfloat *bektoreBerria = (GLfloat*)malloc (sizeof(GLfloat)*4);
    int i,j;
    for(i=0;i<4;i++){
        for(j=0;j<4;j++){
            batura=batura + lag[i+j*4]*bektorea[j];
        }
        bektoreBerria[i]=batura;
        batura=0;

    }
    return bektoreBerria;
}

/* Kamera perspektiba eguneratu*/
void eguneratu_kameraPerspektiba(){

    GLdouble *eyeLag = (GLdouble*)malloc (sizeof(GLdouble)*4);
    eyeLag[0]=0;   eyeLag[1]=0;   eyeLag[2]=0; eyeLag[3]=1;
    GLdouble *centerLag = (GLdouble*)malloc (sizeof(GLdouble)*4);
    centerLag[0]=0;    centerLag[1]=0;    centerLag[2]=30; centerLag[3]=1;
    GLdouble *upLag = (GLdouble*)malloc (sizeof(GLdouble)*4);
    upLag[0]=0;    upLag[1]=1;    upLag[2]=0; upLag[3]=0;
    //Kameraren posizioa eguneratu

    kameraPerspektiba->eye=biderkatuBektorea(eyeLag,kameraPerspektiba->aldaketa_pila);
    kameraPerspektiba->center=biderkatuBektorea(centerLag,kameraPerspektiba->aldaketa_pila);
    kameraPerspektiba->up=biderkatuBektorea(upLag,kameraPerspektiba->aldaketa_pila);

}

/* Kamera ibiltaria eguneratu*/
void eguneratu_kameraIbiltaria(){

    GLdouble *eyeLag = (GLdouble*)malloc (sizeof(GLdouble)*4);
    eyeLag[0]=0;   eyeLag[1]=0;   eyeLag[2]=0; eyeLag[3]=1;
    GLdouble *centerLag = (GLdouble*)malloc (sizeof(GLdouble)*4);
    centerLag[0]=0;    centerLag[1]=0;    centerLag[2]=30; centerLag[3]=1;
    GLdouble *upLag = (GLdouble*)malloc (sizeof(GLdouble)*4);
    upLag[0]=0;    upLag[1]=1;    upLag[2]=0; upLag[3]=0;
    //Kameraren posizioa eguneratu

    kameraIbiltaria->eye=biderkatuBektorea(eyeLag,kameraIbiltaria->aldaketa_pila);
    kameraIbiltaria->center=biderkatuBektorea(centerLag,kameraIbiltaria->aldaketa_pila);
    kameraIbiltaria->up=biderkatuBektorea(upLag,kameraIbiltaria->aldaketa_pila);

}
