#ifndef ARGIAK_H
#define ARGIAK_H

void eguzkiaPiztu();
void bonbilaPiztu();
void fokoaPiztu(GLfloat angelua);
void kobreMateriala();
void jadeMateriala();
void mugituFokua(GLdouble *aldaketa,GLfloat angelua);

#endif // ARGIAK_H
