#include "definitions.h"
#include "load_obj.h"
#include <GL/glut.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


GLdouble* translation_matrix(int x,int y,int z){
	GLdouble *m = (GLdouble*)malloc (sizeof(GLdouble)*16);
	m[0]=1;	m[4]=0;	m[8]=0;	m[12]=x;
	m[1]=0;	m[5]=1;	m[9]=0;	m[13]=y;
	m[2]=0;	m[6]=0;	m[10]=1;m[14]=z;
	m[3]=0;	m[7]=0;	m[11]=0;m[15]=1;
	return m;
}

GLdouble* identity_matrix(){
	GLdouble *m = (GLdouble*)malloc (sizeof(GLdouble)*16);
	m[0]=1;	m[4]=0;	m[8]=0;	m[12]=0;
	m[1]=0;	m[5]=1;	m[9]=0;	m[13]=0;
	m[2]=0;	m[6]=0;	m[10]=1;m[14]=0;
	m[3]=0;	m[7]=0;	m[11]=0;m[15]=1;
	return m;
}

GLdouble* kamera_matrix(){
	GLdouble *m = (GLdouble*)malloc (sizeof(GLdouble)*16);
	m[0]=1;	m[4]=0;	m[8]=0;	m[12]=0;
	m[1]=0;	m[5]=1;	m[9]=0;	m[13]=0;
	m[2]=0;	m[6]=0;	m[10]=1;m[14]=-30;
	m[3]=0;	m[7]=0;	m[11]=0;m[15]=1;
	return m;
}

GLdouble* rotation_matrixX(double x){
	GLdouble *m = (GLdouble*)malloc (sizeof(GLdouble)*16);
	m[0]=1;	m[4]=0;		m[8]=0;		m[12]=0;
	m[1]=0;	m[5]=cos(x);	m[9]=-sin(x);	m[13]=0;
	m[2]=0;	m[6]=sin(x);	m[10]=cos(x);   m[14]=0;
	m[3]=0;	m[7]=0;		m[11]=0;	m[15]=1;
	return m;
}

GLdouble* rotation_matrixY(double y){
	GLdouble *m = (GLdouble*)malloc (sizeof(GLdouble)*16);
	m[0]=cos(y);	m[4]=0;		m[8]=sin(y);	m[12]=0;
	m[1]=0;		m[5]=1;		m[9]=0;		m[13]=0;
	m[2]=-sin(y);	m[6]=0;		m[10]=cos(y);   m[14]=0;
	m[3]=0;		m[7]=0;		m[11]=0;	m[15]=1;
	return m;
}

GLdouble* rotation_matrixZ(double z){
	GLdouble *m = (GLdouble*)malloc (sizeof(GLdouble)*16);
	m[0]=cos(z);	m[4]=-sin(z);	m[8]=0;		m[12]=0;
	m[1]=sin(z);	m[5]=cos(z);	m[9]=0;		m[13]=0;
	m[2]=0;		m[6]=0;		m[10]=1;  	m[14]=0;
	m[3]=0;		m[7]=0;		m[11]=0;	m[15]=1;
	return m;
}

GLdouble* tamaina_matrix(double x,double y,double z){
	GLdouble *m = (GLdouble*)malloc (sizeof(GLdouble)*16);
	m[0]=x;		m[4]=0;		m[8]=0;		m[12]=0;
	m[1]=0;		m[5]=y;		m[9]=0;		m[13]=0;
	m[2]=0;		m[6]=0;		m[10]=z;  	m[14]=0;
	m[3]=0;		m[7]=0;		m[11]=0;	m[15]=1;
	return m;
}


void printMatrix(GLdouble* matrizea){
	int i,j;
	for(i=0;i<4;i++){
		for(j=0;j<4;j++){
			printf("%.3f  ",matrizea[i+4*j]);
		}
		printf("\n");
	}
	printf("\n");
}

