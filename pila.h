#ifndef PILA_H
#define	PILA_H

#include "definitions.h"

pila* eguneratu_objetua(GLdouble *matrix,pila *pilaZaharra);
pila* eguneratu_mundua(GLdouble *matrix,pila *pilaZaharra);


GLdouble* biderkatuBektorea(GLdouble *bektorea,pila *pila);
GLfloat* biderkatuArgia(GLfloat *bektorea,pila *pila);

void eguneratu_kameraPerspektiba();
void eguneratu_kameraIbiltaria();

#endif //PILA_H
